module.exports = (api) => ({
  "presets": [
    [
      "@babel/preset-env",
      {
        "useBuiltIns": false,
      },
    ],
  ],
  "plugins": [
    api.env("development") && "@prefresh/babel-plugin",
    [
      "@babel/plugin-transform-react-jsx",
      {
        "runtime": "automatic",
        "importSource": "preact",
      },
    ],
    [
      "@babel/plugin-transform-typescript",
      {
        "isTSX": true,
        "jsxPragma": "DontPreserveReact",
        "jsxPragmaFrag": "DontPreserveReactFragment",
      },
    ],
  ].filter(Boolean),
})
