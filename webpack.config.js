require('dotenv-safe/config');

const useProdSourceMaps = process.env.USE_PROD_SOURCEMAPS === 'true';
const isProduction = process.env.NODE_ENV === 'production';

const definePluginEnvVars = {'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)};
Object.keys(process.env)
  .filter(k => k.startsWith('APP_'))
  .forEach(k => {
    definePluginEnvVars[`process.env.${k}`] = JSON.stringify(process.env[k]);
  });

let publicPath = process.env.APP_PUBLIC_ROOT_PATH || '/';
if (!publicPath.endsWith('/')) {
  publicPath += '/'
}
definePluginEnvVars['process.env.APP_PUBLIC_ROOT_PATH'] = JSON.stringify(publicPath.slice(0, -1))

const path = require('path');
const webpack = require('webpack');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const TsCheckerPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
// const HtmlScriptPlugin = require('script-ext-html-webpack-plugin');
const HtmlFaviconsPlugin = require('favicons-webpack-plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');
// const HtmlCssPlugin = require('style-ext-html-webpack-plugin');
const MinifyJsPlugin = require('terser-webpack-plugin');
const MinifyCssPlugin = require('css-minimizer-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { GenerateSW } = require('workbox-webpack-plugin');
const PreactRefreshPlugin = require('@prefresh/webpack');

const devOrProd = (dev, prod) => isProduction ? prod : dev;

const cssLoaders = withModules => [
  {
    loader: devOrProd('style-loader', CssExtractPlugin.loader),
  },
  (withModules ?
    {
      loader: 'css-loader',
      options: {
        sourceMap: true,
        modules: {
          localIdentName: devOrProd('[path][name]__[local]__[contenthash:base64:8]', '[contenthash:base64:8]'),
          localIdentContext: path.resolve(__dirname, 'src'),
        },
        importLoaders: 3,
      },
    } : {
      loader: 'css-loader',
      options: {
        sourceMap: true,
        modules: false,
        importLoaders: 3,
      },
    }
  ),
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
      postcssOptions: {
        plugins: [
          'postcss-flexbugs-fixes',
          'autoprefixer',
          ['postcss-normalize', { forceImport: 'sanitize.css' }],
        ],
      }
    },
  },
  {
    loader: 'resolve-url-loader',
    options: {
      sourceMap: true,
    }
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      additionalData: '@use "utils/_vars" as *;',
      sassOptions: {
        includePaths: [path.resolve(__dirname, 'src')],
      },
    },
  },
]

module.exports = {
  mode: devOrProd('development', 'production'),
  target: devOrProd("web", "browserslist"),
  bail: devOrProd(undefined, true),
  devtool: devOrProd('cheap-module-source-map', useProdSourceMaps && 'source-map'),
  output: {
    path: devOrProd(undefined, path.resolve(__dirname, 'build')),
    publicPath,
    filename: devOrProd(
      'static/[name].bundle.js',
      c => 'static/[contenthash:8].js'), //c.chunk.name === 'webpackruntime' ? 'webpackruntime.js' : 'static/[contenthash:8].js'),
    chunkFilename: devOrProd('static/[name].chunk.js', 'static/[contenthash:8].js'),
  },
  devServer: devOrProd({
    contentBase: false,
    port: 3000,
    historyApiFallback: { // redirect all plain GETs to index.html
      disableDotRule: true,
      index: `${publicPath}index.html`,
    },
    hot: true,
    overlay: { // full screen overlay in browser
      errors: true,
      warnings: false,
    },
    stats: 'normal', // messages shown in build console
    clientLogLevel: 'info', // messages shown in client web console
  }, undefined),
  entry: {
    main: path.resolve(__dirname, 'src/index.tsx')
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    plugins: [
      new TsConfigPathsPlugin(),
    ],
    alias: {
      "react": "preact/compat",
      "react-dom": "preact/compat",
    }
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.tsx?$/,
        exclude:  /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          cacheCompression: false,
        }
      },
      devOrProd(false, {
        test: /\.js$/,
        include: /node_modules/,
        exclude: /[\\/]node_modules[\\/](?!(wouter-preact)[\\/])/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          cacheCompression: false,
          configFile: false,
          babelrc: false,
          presets: [
            [
              '@babel/preset-env',
              {
                useBuiltIns: false,
              },
            ],
          ],
        },
      }),
      {
        test: /\.s?css$/,
        oneOf: [
          {
            resourceQuery: /global/,
            use: cssLoaders(false),
            sideEffects: true,
          },
          {
            use: cssLoaders(true),
          }
        ],
      },
      {
        test: /\.svg$/,
        oneOf: [
          {
            resourceQuery: /inline/,
            use: [
              {
                loader: 'babel-loader',
                options: {
                  cacheDirectory: true,
                  cacheCompression: false,
                },
              },
              {
                loader: 'babel-loader',
                options: {
                  babelrc: false,
                  plugins: [
                    [
                      path.resolve(__dirname, 'config', 'rename-jsx-attribute-name.js'),
                      {
                        rename: {
                          'strokeWidth': 'stroke-width',
                          'fontSize': 'font-size',
                          'fontWeight': 'font-weight',
                          'fontFamily': 'font-family',
                          'textAnchor': 'text-anchor',
                        },
                      },
                    ],
                  ],
                },
              },
              {
                loader: '@svgr/webpack',
                options: { babel: false },
              },
            ],
          },
          {
            type: 'asset',
            generator: {
              filename: 'static/[contenthash:8][ext]'
            },
            parser: {
              dataUrlCondition: {
                maxSize: 4 * 1024
              }
            }
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        type: 'asset',
        generator: {
          filename: 'static/[contenthash:8][ext]'
        },
        parser: {
          dataUrlCondition: {
            maxSize: 4 * 1024
          }
        }
      },
    ].filter(Boolean),
  },
  plugins: [
    devOrProd(new PreactRefreshPlugin(), false),
    devOrProd(false, new CleanWebpackPlugin()),
    new webpack.DefinePlugin(definePluginEnvVars),
    new TsCheckerPlugin({
      typescript: {
        mode: 'write-references'
      },
      eslint: {
        files: path.resolve(__dirname, 'src/**/*.{ts,tsx,js,jsx}')
      }
    }),
    new HtmlPlugin(devOrProd({
      template: './src/index.html',
    },{
      template: './src/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        removeScriptTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    })),
    // devOrProd(false, new HtmlScriptPlugin({ // not supported on webpack 5
    //   inline: ['webpackruntime.js'],
    //   defaultAttribute: 'defer',
    // })),
    new HtmlFaviconsPlugin({
      logo: path.resolve(__dirname, 'src', 'favicon.svg'),
      cache: true,
      inject: true,
      prefix: 'static/[contenthash:8]/',
      favicons: {
        appName: 'Offline-maastokartta DEMO',
        appDescription: 'Proof of concept web-karttasovellus, jolla voi tallentaa karttatiilet käytettäväksi offline-tilassa.',
        developerName: null,
        developerURL: null,
        version: null,
        lang: 'fi',
        scope: publicPath,
        start_url: `${publicPath}kartta`,
        theme_color: '#586079',
        background: '#fff',
        icons: {
          favicons: true,
          android: true,
          appleIcon: true,
          appleStartup: true,
          firefox: false,
          windows: false,
          coast: false,
          yandex: false,
        },
      }
    }),
    devOrProd(false, new CssExtractPlugin({
      filename: 'static/[contenthash:8].css',
      chunkFilename: 'static/[contenthash:8].css',
    })),
    // devOrProd(false, new HtmlCssPlugin()), // not supported on webpack 5
    devOrProd(false, new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
      reportFilename: 'analyzer/analyzer.html',
    })),
    devOrProd(false, new GenerateSW({
      swDest: 'service-worker.js',
      navigateFallback: `${publicPath}index.html`,
      navigateFallbackDenylist: [
        /\/static\//,
      ],
      dontCacheBustURLsMatching: /static\/[0-9a-f]{8}\//,
      exclude: [/\.map$/, /LICENSE/, /apple-touch-/, /android-chrome-/],
      runtimeCaching: [
        {
          urlPattern: /\/static\//,
          handler: 'NetworkFirst',
          options: {
            cacheName: 'non-precached-static'
          }
        },
        {
          urlPattern: /^https:\/\/unpkg\.com\/leaflet/,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'non-precached-static'
          }
        },
        {
          urlPattern: /^https:\/\/fonts\.googleapis\.com/,
          handler: 'StaleWhileRevalidate',
          options: {
            cacheName: 'google-fonts-stylesheets'
          }
        },

        {
          urlPattern: /^https\:\/\/fonts\.gstatic\.com/,
          handler: 'CacheFirst',
          options: {
            cacheName: 'google-fonts-webfonts'
          }
        },
      ],
    })),
  ].filter(Boolean),
  optimization: devOrProd(undefined, {
    runtimeChunk: {
      name: 'webpackruntime',
    },
    splitChunks: {
      chunks: 'all',
    },
    removeAvailableModules: true,
    nodeEnv: false,
    minimize: true,
    minimizer: [
      new MinifyJsPlugin({
        terserOptions: {
          parse: {
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2,
          },
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true,
          },
        },
      }),
      new MinifyCssPlugin({
        sourceMap: useProdSourceMaps ? {
          inline: false,
          annotation: true,
        } : false,
        minimizerOptions: {
          preset: ['default', { minifyFontValues: { removeQuotes: false } }],
        },
      }),
    ],
  }),
  performance: {
    hints: devOrProd(undefined, 'error'),
  },
};
