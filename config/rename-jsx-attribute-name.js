const renameJSXAttribute = ({ types: t }, opts) => {

  const toRename = opts.rename;
  const originalNames = Object.keys(toRename);

  return {
    inherits: require('@babel/plugin-syntax-jsx').default,
    visitor: {
      JSXAttribute(path) {
        const attributeNamePath = path.get('name');
        if (t.isJSXIdentifier(attributeNamePath)) {
          const originalName = attributeNamePath.node.name;
          if (originalNames.includes(originalName)) {
            attributeNamePath.replaceWith(t.jsxIdentifier(toRename[originalName]));
          }
        }
      },
    }
  };
};

module.exports = renameJSXAttribute;
