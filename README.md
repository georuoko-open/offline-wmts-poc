# offline-wmts-poc

Simple proof-of-concept Preact application that displays a Leaflet map with a custom WMTS layer.

## Custom tile layer

Instead of the layer using the default `img` element with WMTS url as the src, layer fetches the tile from indexeddb or the WMTS url, and uses the resulting tile image blob as src with `URL.createObjectUrl`. For this to work, the WMTS needs to be same-origin or support CORS.

## Caching

Caching logic takes the current map view, calculates the tiles needed to show the current view in every zoom level, and puts the selection to localstorage. Downloader logic takes the saved areas, fetches the missing tiles and puts the image blobs to indexeddb for the custom tile layer to find. Logic is very basic and if the caching throws, page needs to be refreshed to start it again.

Caching is also done on-the-fly when browsing the map, since the blob is available anyway. These are not shown on the saved areas list so you need to clear the indexeddb storage to get rid of these tiles if necessary.

## Demo

Demo of this repo published with GitLab pages is available at [https://georuoko-open.gitlab.io/offline-wmts-poc](https://georuoko-open.gitlab.io/offline-wmts-poc). Contents are in Finnish and the demo uses [NLS Open Data API WMTS service](https://www.maanmittauslaitos.fi/en/maps-and-spatial-data/expert-users/topographic-data-and-how-acquire-it).

## Running locally

Demo is only configured to use a terrain map of Finland with CRS EPSG:3067 tile set. Clone the repo to test your config with the concept, for example to use EPSG:3857 OSM tiles.

* Use `npm start` and `http://localhost:3000` with hot reloading enabled.
* Use `npm run build` to generate the bundle.
