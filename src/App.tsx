import { FC } from 'types';
import { useEffect, useState } from 'preact/hooks';
import { Link, useLocation, Route, Switch, Router } from 'wouter-preact';
import { useMeta, useTitleTemplate, useTitle } from 'hoofd/preact';
import cn from 'classnames';
import Routes from 'config/routes';
import NavLink from 'components/routing/NavLink';
import PagePrerenderBoundary from 'utils/PagePrerenderBoundary';
import Home from 'pages/Home';
import Settings from 'pages/Settings';
import Map from 'pages/Map';
import Attribution from 'pages/Attribution';
import NotFound from 'components/routing/NotFound';
import SettingsProvider from 'components/settings/SettingsProvider';
import TileCacheSettingsProvider from 'components/tilecache/TileCacheSettingsProvider';

import OfflineMarkerIcon from 'icons/offline-marker-icon.svg?inline';
import BurgerMenuIcon from 'icons/burger-menu-icon.svg?inline';
import BurgerMenuIconClose from 'icons/burger-menu-icon-close.svg?inline';

import s from './App.scss';

const Header: FC = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const [location] = useLocation();
  useEffect(() => setMenuOpen(false), [location]);

  return (
    <header className={s.header}>
      <Link
        href={Routes.HOME}
        className={s.frontpageLink}
        aria-label="Etusivulle"
        onClick={() => setMenuOpen(false)}
      >
        <OfflineMarkerIcon aria-hidden={true} />
        Offline-maastokartta DEMO
      </Link>
      <button
        aria-hidden={true}
        className={s.navMenuButton}
        onClick={() => setMenuOpen((o) => !o)}
      >
        {menuOpen ? <BurgerMenuIconClose /> : <BurgerMenuIcon />}
      </button>
      <nav className={cn(s.navLinks, { [s.hidden]: !menuOpen })}>
        <ul>
          <li>
            <NavLink
              href={Routes.SETTINGS}
              activeClassName={s.active}
              onClick={() => setMenuOpen(false)}
            >
              Asetukset
            </NavLink>
          </li>
          <li>
            <NavLink
              href={Routes.MAP}
              activeClassName={s.active}
              onClick={() => setMenuOpen(false)}
            >
              Kartta
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

const RouteTitle: FC<{ title: string }> = ({ title }) => {
  useTitle(title);
  return null;
};

const MainRouter: FC = () => (
  <Router>
    <Switch>
      <Route path={Routes.HOME}>
        <RouteTitle title="Etusivu" />
        <PagePrerenderBoundary>
          <Home />
        </PagePrerenderBoundary>
      </Route>
      <Route path={Routes.SETTINGS}>
        <RouteTitle title="Asetukset" />
        <PagePrerenderBoundary>
          <Settings />
        </PagePrerenderBoundary>
      </Route>
      <Route path={Routes.MAP}>
        <RouteTitle title="Kartta" />
        <PagePrerenderBoundary>
          <Map />
        </PagePrerenderBoundary>
      </Route>
      <Route path={Routes.ATTRIBUTION}>
        <RouteTitle title="Lähteet" />
        <PagePrerenderBoundary>
          <Attribution />
        </PagePrerenderBoundary>
      </Route>
      <Route>
        <RouteTitle title="Sivua ei löytynyt | 404" />
        <NotFound />
      </Route>
    </Switch>
  </Router>
);

const App: FC = () => {
  useTitleTemplate('%s | Offline-maastokartta DEMO');
  useMeta({
    name: 'description',
    content:
      'Proof of concept web-karttasovellus, jolla voi tallentaa karttatiilet käytettäväksi offline-tilassa.',
  });

  return (
    <div>
      <SettingsProvider>
        <TileCacheSettingsProvider>
          <Header />
          <MainRouter />
        </TileCacheSettingsProvider>
      </SettingsProvider>
    </div>
  );
};

export default App;
