if (process.env.NODE_ENV === 'development') {
  require('preact/debug');
}

import './index.scss?global';

import { render } from 'preact';
import App from 'App';

render(<App />, document.body, document.body.firstElementChild as Element);

if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker
      .register(`${process.env.APP_PUBLIC_ROOT_PATH}/service-worker.js`)
      .catch(() => {
        // swallow registration errors
      });
  });
}
