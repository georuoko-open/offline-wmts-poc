import { FC } from 'types';
import { Link } from 'wouter-preact';
import Routes from 'config/routes';
import { useSettings } from 'components/settings/SettingsProvider';
import MapContainer from 'components/map/MapContainer';

import us from 'utils/helpers.scss';

const Map: FC = () => {
  const { apikey } = useSettings();

  if (!apikey) {
    return (
      <main className={us.defaultBodyPadding}>
        <h1>Kartta</h1>
        <p>
          Aseta karttakuvapalvelun rajapinnan avain ensin{' '}
          <Link href={Routes.SETTINGS}>asetuksissa</Link>.
        </p>
      </main>
    );
  }

  return (
    <main
      style={{
        position: 'absolute',
        top: '50px',
        left: '0',
        height: 'calc(100% - 50px)',
        width: '100%',
      }}
    >
      <h1 className={us.srOnly}>Kartta</h1>
      <MapContainer />
    </main>
  );
};

export default Map;
