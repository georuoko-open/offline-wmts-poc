import { FC } from 'types';

import us from 'utils/helpers.scss';

const Attribution: FC = () => {
  return (
    <main className={us.defaultBodyPadding}>
      <h1>Lähteet</h1>
      <h2>Maanmittauslaitoksen Karttakuvapalvelu</h2>
      <p>Palvelu sisältää Maanmittauslaitoksen Karttakuvapalvelun aineistoa.</p>
      <p>
        Lisätietoja:
        <ul>
          <li>
            <a
              href="https://www.maanmittauslaitos.fi/avoindata-lisenssi-cc40"
              target="_blank"
              rel="noopener noreferrer"
            >
              Maanmittauslaitoksen avoimen tietoaineiston lisenssi
            </a>
          </li>
          <li>
            <a
              href="https://www.maanmittauslaitos.fi/karttakuvapalvelu/tekninen-kuvaus-wmts#avoin-rajapintayhteys"
              target="_blank"
              rel="noopener noreferrer"
            >
              Maanmittauslaitoksen karttakuvapalvelu
            </a>
          </li>
        </ul>
      </p>
    </main>
  );
};

export default Attribution;
