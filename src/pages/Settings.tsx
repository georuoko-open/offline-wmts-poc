import { FC } from 'types';
import SettingsForm from 'components/settings/SettingsForm';
import TileCacheSettingsForm from 'components/tilecache/TileCacheSettingsForm';

import us from 'utils/helpers.scss';

const Settings: FC = () => {
  return (
    <main className={us.defaultBodyPadding}>
      <h1>Asetukset</h1>
      <SettingsForm />
      <TileCacheSettingsForm />
    </main>
  );
};

export default Settings;
