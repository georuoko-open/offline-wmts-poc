import { FC } from 'types';

import us from 'utils/helpers.scss';

const Home: FC = () => {
  return (
    <main className={us.defaultBodyPadding}>
      <h1>Offline-maastokartta DEMO</h1>
      <p>
        HUOM: Tämä on vain teknologiademo, joten älä luota tähän ainoana
        karttana maastossa.
      </p>
      <p>
        Proof of concept web-karttasovellus, jolla voi tallentaa karttatiilet
        käytettäväksi offline-tilassa. Demoon on konfiguroitu valmiiksi
        Maanmittauslaitoksen Karttakuvapalvelun avoimen rajapinnan maastokartta,
        testailuun tarvitset vain oman rajapinta-avaimen.
      </p>
      <p>
        Sovelluksella on tarkoitus testata yksinkertaista web-clienttia
        WMTS-karttatiilipalvelua varten, tallennustapa käyttäjän laitteella on
        asetuksille localstorage ja karttatiilille indexeddb. Sovellus
        rakennettu Preact + Leaflet avulla. Lisätietoja ja lähdekoodit{' '}
        <a
          href="https://gitlab.com/georuoko-open/offline-wmts-poc"
          target="_blank"
          rel="noopener noreferrer"
        >
          GitLabissa
        </a>
        .
      </p>
    </main>
  );
};

export default Home;
