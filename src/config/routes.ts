const Routes = Object.fromEntries(
  Object.entries({
    HOME: '/',
    MAP: '/kartta',
    SETTINGS: '/asetukset',
    ATTRIBUTION: '/lahteet',
  }).map(([n, p]) => [n, `${process.env.APP_PUBLIC_ROOT_PATH}${p}`]),
);

export default Routes;
