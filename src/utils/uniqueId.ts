const uniqueId = (prefix?: string): string =>
  `${prefix || ''}${Math.random().toString(36).substring(7)}`;

export default uniqueId;
