import { FC } from 'types';
import Loader from 'components/ui/Loader';

import s from './PagePrerenderBoundary.scss';

const PagePrerenderBoundary: FC = ({ children }) => {
  return process.env.NODE_ENV === 'production' &&
    navigator.userAgent === 'ReactSnap' ? (
    <div className={s.wrapper}>
      <Loader className={`hidenojs ${s.loader}`} delayVisibility={true} />
      <noscript>
        <div className={s.noJsBlock}>
          <p>Ota JavaScript käyttöön.</p>
        </div>
      </noscript>
    </div>
  ) : (
    <>{children}</>
  );
};

export default PagePrerenderBoundary;
