declare module '*.svg?inline' {
  import { FunctionComponent, JSX } from 'preact';
  const SvgComponent: FunctionComponent<JSX.IntrinsicElements['svg']>;
  export default SvgComponent;
}

declare module '*.svg' {
  const sourceUrl: string;
  export default sourceUrl;
}

declare module '*.scss?global';
declare module '*.scss' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module '*.png' {
  const sourceUrl: string;
  export default sourceUrl;
}

declare module '*.gif' {
  const sourceUrl: string;
  export default sourceUrl;
}

declare module '*.jpg' {
  const sourceUrl: string;
  export default sourceUrl;
}

declare module '*.jpeg' {
  const sourceUrl: string;
  export default sourceUrl;
}
