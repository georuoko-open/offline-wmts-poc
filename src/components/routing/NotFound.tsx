import { FC } from 'types';

import us from 'utils/helpers.scss';

const NotFound: FC = () => {
  return (
    <main className={us.defaultBodyPadding}>
      <h1>Sivua ei löytynyt</h1>
    </main>
  );
};

export default NotFound;
