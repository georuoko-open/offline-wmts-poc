import { FC } from 'types';
import cn from 'classnames';
import { Link, useRoute, LinkProps } from 'wouter-preact';

const NavLink: FC<
  Omit<LinkProps, 'href' | 'to'> & { href: string; activeClassName?: string }
> = (props) => {
  const { activeClassName = 'active', href, className, ...rest } = props;

  const [isActive] = useRoute(href);

  return (
    <Link
      className={cn(className, { [activeClassName]: isActive })}
      href={href}
      {...rest}
    />
  );
};

export default NavLink;
