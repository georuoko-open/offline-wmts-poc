import { FC } from 'types';
import { useState } from 'preact/hooks';
import { Popup, Rectangle } from 'react-leaflet';
import { useTileCacheAreas } from 'components/tilecache/TileCacheSettingsProvider';

import DownloadDoneIcon from 'icons/download-done.svg?inline';

import s from './ShowCachedAreasButton.scss';

const ShowCachedAreasButton: FC = () => {
  const [enabled, setEnabled] = useState(false);
  const areas = useTileCacheAreas();

  if (areas.length === 0) {
    return null;
  }

  return (
    <>
      <div className={s.wrapper}>
        <button
          onClick={() =>
            setEnabled((e) => {
              return !e;
            })
          }
        >
          <DownloadDoneIcon fill={enabled ? 'green' : 'grey'} />
        </button>
      </div>
      {enabled &&
        areas.map(({ id, name, timestamp, bounds: { n, w, s, e } }) => (
          <Rectangle
            key={id}
            bounds={[
              [n, w],
              [s, e],
            ]}
            stroke={true}
            weight={3}
            color="black"
            fill={true}
            fillColor="blue"
            fillOpacity={0.15}
          >
            <Popup>
              {name} (tallennettu {timestamp})
            </Popup>
          </Rectangle>
        ))}
    </>
  );
};

export default ShowCachedAreasButton;
