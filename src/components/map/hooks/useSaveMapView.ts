import { useCallback, useMemo } from 'preact/hooks';
import { LatLng, LatLngTuple } from 'leaflet';
import { useMap, useMapEvents } from 'react-leaflet';
import { MAP_DEFAULT_LOCATION } from '../config';

const loadLocation = () => {
  try {
    const storedLocation = window.localStorage.getItem('location');
    if (storedLocation) {
      const { lat, lng, zoom } = JSON.parse(storedLocation) as {
        lat: number;
        lng: number;
        zoom: number;
      };
      return { center: [lat, lng] as LatLngTuple, zoom };
    }
  } catch (error) {
    // swallow errors
  }
  return MAP_DEFAULT_LOCATION;
};

const saveLocation = (center: LatLng, zoom: number): void => {
  try {
    window.localStorage.setItem(
      'location',
      JSON.stringify({ lat: center.lat, lng: center.lng, zoom }),
    );
  } catch (error) {
    // swallow errors
  }
};

export const usePreviouslySavedMapView = (): {
  center: LatLngTuple;
  zoom: number;
} => loadLocation();

export const useSaveMapView = (): void => {
  const map = useMap();
  const handler = useCallback(() => {
    saveLocation(map.getCenter(), map.getZoom());
  }, [map]);
  const handlers = useMemo(() => ({ moveend: handler, zoomend: handler }), [
    handler,
  ]);
  useMapEvents(handlers);
};
