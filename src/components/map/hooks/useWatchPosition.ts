import { LatLngTuple } from 'leaflet';
import { useCallback, useEffect, useState } from 'preact/hooks';
import { latLngToEtrsTm35FinNE } from '../config';

type SavedPosition = {
  coords: LatLngTuple;
  coordsWgs84: LatLngTuple;
  accuracy: number;
};

const useWatchPosition = (
  enabled: boolean,
  positionOptions?: PositionOptions,
): { position: SavedPosition | null; isError: boolean } => {
  const [position, setPosition] = useState<SavedPosition | null>(null);
  const [error, setError] = useState<GeolocationPositionError | null>(null);

  const onPositionRecieved = useCallback((p: GeolocationPosition) => {
    setError(null);
    const {
      coords: { latitude, longitude, accuracy },
    } = p;
    const [n, e] = latLngToEtrsTm35FinNE(latitude, longitude);
    const lat = +latitude.toFixed(5);
    const lon = +longitude.toFixed(5);
    const a = Math.round(accuracy);
    setPosition((op) => {
      if (!op) {
        return {
          coords: [n, e],
          coordsWgs84: [lat, lon],
          accuracy: a,
        };
      }
      const {
        coords: [on, oe],
        accuracy: oa,
      } = op;
      if (on !== n || oe !== e || oa !== a) {
        return {
          coords: [n, e],
          coordsWgs84: [lat, lon],
          accuracy: a,
        };
      }
      return op;
    });
  }, []);

  useEffect(() => {
    if (enabled) {
      try {
        const watch = navigator.geolocation.watchPosition(
          onPositionRecieved,
          setError,
          positionOptions,
        );
        return () => navigator.geolocation.clearWatch(watch);
      } catch (e) {
        setError(new GeolocationPositionError());
      }
    }
    return;
  }, [enabled, onPositionRecieved, positionOptions]);

  return {
    position: position,
    isError: !!error,
  };
};

export default useWatchPosition;
