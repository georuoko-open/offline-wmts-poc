import { FC } from 'types';
import { useEffect, useState } from 'preact/hooks';
import { Circle, CircleMarker, useMap } from 'react-leaflet';
import useWatchPosition from '../hooks/useWatchPosition';
import Loader from 'components/ui/Loader';
import PositionInfoModal from './PositionInfoModal';

import LocationOnIcon from 'icons/location-on.svg?inline';
import LocationOffIcon from 'icons/location-off.svg?inline';
import LockClosedIcon from 'icons/lock-closed.svg?inline';
import LockOpenIcon from 'icons/lock-open.svg?inline';
import LocationInfoIcon from 'icons/location-info.svg?inline';

import s from './FollowPositionButton.scss';

const FollowPositionButton: FC = () => {
  const [enabled, setEnabled] = useState(false);
  const [followed, setFollowed] = useState(false);
  const { position, isError } = useWatchPosition(enabled, {
    enableHighAccuracy: true,
  });
  const [modalOpen, setModalOpen] = useState(false);

  const map = useMap();

  useEffect(() => {
    if (enabled && followed && !!position) {
      map.setView(position.coords);
    }
  }, [position, enabled, followed, map]);

  return (
    <>
      <div className={s.wrapper}>
        <button
          onClick={() =>
            setEnabled((e) => {
              if (e) {
                setFollowed(false);
              }
              return !e;
            })
          }
        >
          {!enabled ? (
            <LocationOffIcon fill="grey" />
          ) : !position && !isError ? (
            <Loader />
          ) : (
            <LocationOnIcon fill={!isError ? 'green' : 'red'} />
          )}
        </button>
        {enabled && (
          <button onClick={() => setFollowed((f) => !f)}>
            {!followed ? (
              <LockOpenIcon fill="grey" />
            ) : (
              <LockClosedIcon fill="green" />
            )}
          </button>
        )}
        {enabled && (
          <button onClick={() => setModalOpen(true)}>
            <LocationInfoIcon fill="grey" />
          </button>
        )}
      </div>
      {modalOpen && <PositionInfoModal onClose={() => setModalOpen(false)} />}
      {enabled && !!position && (
        <>
          {position.accuracy >= 5 && (
            <Circle
              center={position.coords}
              radius={position.accuracy}
              color="#4a4a4a"
              weight={1}
              opacity={0.5}
              fillColor="#0a0dba"
              fillOpacity={0.15}
              interactive={false}
            />
          )}
          <CircleMarker
            center={position.coords}
            radius={8}
            color="#8a8a8a"
            weight={1}
            fillColor="#0a0dba"
            fillOpacity={1}
            interactive={false}
          />
        </>
      )}
    </>
  );
};

export default FollowPositionButton;
