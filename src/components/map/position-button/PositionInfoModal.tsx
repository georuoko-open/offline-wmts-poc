import { FC } from 'types';
import { createPortal } from 'preact/compat';
import { LatLngTuple } from 'leaflet';
import useWatchPosition from 'components/map/hooks/useWatchPosition';

import CloseIcon from 'icons/close.svg?inline';

import s from './PositionInfoModal.scss';

const degInDegMinFormat = (coordsWgs84: LatLngTuple): string => {
  const [lat, lon] = coordsWgs84;
  const latChar = lat >= 0 ? 'N' : 'S';
  const lonChar = lon >= 0 ? 'E' : 'W';
  const latDegs = Math.floor(lat).toFixed(0).padStart(2, '0');
  const latMins = ((lat % 1) * 60).toFixed(4);
  const lonDegs = Math.floor(lon).toFixed(0).padStart(3, '0');
  const lonMins = ((lon % 1) * 60).toFixed(4);
  return `${latChar} ${latDegs}° ${latMins}', ${lonChar} ${lonDegs}° ${lonMins}'`;
};

const PositionInfoModal: FC<{
  onClose: () => void;
}> = ({ onClose }) => {
  const { position, isError } = useWatchPosition(true, {
    enableHighAccuracy: true,
  });

  return createPortal(
    <div className={s.backdrop} onClick={onClose}>
      <div className={s.modal} onClick={(e) => e.stopPropagation()}>
        <div className={s.banner}>
          <h2>Sijaintitiedot</h2>
          <button onClick={onClose} aria-label="Sulje ikkuna">
            <CloseIcon />
          </button>
        </div>
        <div>
          {!position && !isError ? (
            <p>Ladataan...</p>
          ) : !position ? (
            <p>Sijainti ei ole käytettävissä.</p>
          ) : (
            <>
              <p>
                ETRS-TM35FIN: N {position.coords[0].toFixed(0)}, E{' '}
                {position.coords[1].toFixed(0)}
              </p>
              <p>WGS84: {degInDegMinFormat(position.coordsWgs84)}</p>
              <p>
                Sijainnin epävarmuus on noin {position.accuracy.toFixed(0)}{' '}
                metriä.
              </p>
            </>
          )}
        </div>
      </div>
    </div>,
    document.body,
  );
};

export default PositionInfoModal;
