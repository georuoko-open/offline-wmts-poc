import { FC } from 'types';
import { useLink } from 'hoofd/preact';
import { useState } from 'preact/hooks';
import { MapContainer as LeafletMapContainer } from 'react-leaflet';
import {
  EPSG_3067_CRS,
  EPSG_3067_CRS_TILESET_BOUNDS,
  generateMapUrl,
  MAP_URL_MAX_NATIVE_ZOOM_LEVEL,
} from './config';
import {
  usePreviouslySavedMapView,
  useSaveMapView,
} from './hooks/useSaveMapView';
import { useSettings } from 'components/settings/SettingsProvider';
import CacheableTileLayer from './cacheable-tilelayer/CacheableTileLayer';
import FollowPositionButton from './position-button/FollowPositionButton';
import ShowCachedAreasButton from './cached-areas-button/ShowCachedAreasButton';
import AddToTileCacheButton from './add-to-cache-button/AddToTileCacheButton';
import ScaleBar from './scalebar/ScaleBar';
import LockZoomButton from './lock-zoom-button/LockZoomButton';
import RoutesButton from './routes/RoutesButton';
import SavedRoutesProvider from './routes/SavedRoutesProvider';
import SavedRoutesLayer from './routes/SavedRoutesLayer';

const MapViewSaver: FC = () => {
  useSaveMapView();
  return null;
};

const MapContainer: FC = () => {
  useLink({
    rel: 'stylesheet',
    href: 'https://unpkg.com/leaflet@1.8.0/dist/leaflet.css',
    crossorigin: 'anonymous',
  });

  const { apikey } = useSettings();
  const savedMapView = usePreviouslySavedMapView();

  const [lockedZoom, setLockedZoom] = useState<number | null>(null);
  const zoomProps =
    lockedZoom != null
      ? { maxNativeZoom: lockedZoom, minNativeZoom: lockedZoom }
      : { maxNativeZoom: MAP_URL_MAX_NATIVE_ZOOM_LEVEL };

  return (
    <>
      <LeafletMapContainer
        style={{ height: '100%', width: '100%' }}
        crs={EPSG_3067_CRS}
        maxBounds={EPSG_3067_CRS_TILESET_BOUNDS}
        center={savedMapView.center}
        zoom={savedMapView.zoom}
      >
        <MapViewSaver />
        <LockZoomButton lockedZoom={lockedZoom} setLockedZoom={setLockedZoom} />
        <CacheableTileLayer
          attribution={`&copy; <a href="${process.env.APP_PUBLIC_ROOT_PATH}/lahteet">Maanmittauslaitos<a>`}
          url={generateMapUrl(apikey)}
          {...zoomProps}
        />
        <FollowPositionButton />
        <ShowCachedAreasButton />
        <AddToTileCacheButton />
        <SavedRoutesProvider>
          <RoutesButton />
          <SavedRoutesLayer />
        </SavedRoutesProvider>
        <ScaleBar />
      </LeafletMapContainer>
    </>
  );
};

export default MapContainer;
