import { FC } from 'types';
import { useEffect } from 'preact/hooks';
import { useMap } from 'react-leaflet';
import { control } from 'leaflet';

const ScaleBar: FC = () => {
  const map = useMap();

  useEffect(() => {
    const s = control.scale({
      imperial: false,
      metric: true,
      position: 'bottomright',
    });
    s.addTo(map);
    return () => {
      s.remove();
    };
  }, [map]);

  return null;
};

export default ScaleBar;
