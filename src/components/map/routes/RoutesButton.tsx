import { FC } from 'types';
import { useState } from 'preact/hooks';

import RoutesModal from './RoutesModal';
import RoutesIcon from 'icons/route.svg?inline';

import s from './RoutesButton.scss';

const RoutesButton: FC = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <div className={s.wrapper}>
        <button onClick={() => setIsModalOpen(true)}>
          <RoutesIcon />
        </button>
      </div>
      {isModalOpen && <RoutesModal onClose={() => setIsModalOpen(false)} />}
    </>
  );
};

export default RoutesButton;
