import { useEffect, useState } from 'preact/hooks';
import { useMap } from 'react-leaflet';

import { Polyline, LatLng, Marker, Tooltip } from 'leaflet';

type RouteCoords = [number, number, string][];

type DrawingCallbacks = {
  addPoint: () => void;
  undoPoint: () => void;
  addNote: (text: string) => void;
  getCoords: () => RouteCoords;
  getLastNote: () => string | null;
  distance: number;
  pendingDistance: number;
};

const useDrawOnMap = (coords: RouteCoords): [number, number] => {
  const map = useMap();
  const [polyline] = useState(
    () =>
      new Polyline(
        Array.from(coords, ([lat, lng]) => [lat, lng]),
        { color: 'red' },
      ),
  );
  const [pendingPolyline] = useState(
    () => new Polyline([], { color: 'red', dashArray: '5 10' }),
  );

  const [pendingDistance, setPendingDistance] = useState(0);

  useEffect(() => {
    const onMove = () => {
      const drawn = polyline.getLatLngs();
      const lastDrawn = drawn[drawn.length - 1] as LatLng | undefined;
      if (lastDrawn) {
        const center = map.getCenter();
        pendingPolyline.setLatLngs([lastDrawn, center]);
        const { lat, lng } = lastDrawn;
        setPendingDistance(
          Math.sqrt((lat - center.lat) ** 2 + (lng - center.lng) ** 2),
        );
      } else if (pendingPolyline.getLatLngs().length) {
        pendingPolyline.setLatLngs([]);
      }
    };

    polyline.addTo(map);
    pendingPolyline.addTo(map);
    map.addEventListener('move', onMove);

    return () => {
      polyline.removeFrom(map);
      pendingPolyline.removeFrom(map);
      map.removeEventListener('move', onMove);
    };
  }, [map, polyline, pendingPolyline]);

  useEffect(() => {
    polyline.setLatLngs(Array.from(coords, ([lat, lng]) => [lat, lng]));
    if (coords.length) {
      const center = map.getCenter();
      const [lat, lng] = coords[coords.length - 1];
      pendingPolyline.setLatLngs([
        [lat, lng],
        [center.lat, center.lng],
      ]);
      setPendingDistance(
        Math.sqrt((lat - center.lat) ** 2 + (lng - center.lng) ** 2),
      );
    } else {
      pendingPolyline.setLatLngs([]);
      setPendingDistance(0);
    }
  }, [map, polyline, pendingPolyline, coords]);

  const d = coords.reduce((s, [x, y], i, cs) => {
    if (i === 0) {
      return 0;
    }
    const [px, py] = cs[i - 1];
    return s + Math.sqrt((x - px) ** 2 + (y - py) ** 2);
  }, 0);

  return [d, pendingDistance];
};

const useDrawWaypointsOnMap = (waypoints: RouteCoords) => {
  const map = useMap();

  useEffect(() => {
    const markers = Array.from(
      waypoints.filter(([_lat, _lng, text]) => text != ''),
      ([lat, lng, text]) => {
        const m = new Marker([lat, lng]);
        const tt = new Tooltip({ interactive: false, permanent: true });
        m.bindTooltip(tt);
        m.setTooltipContent(text);
        return m;
      },
    );

    markers.forEach((m) => m.addTo(map));
    markers.forEach((m) => m.openTooltip());

    return () => {
      markers.forEach((m) => m.removeFrom(map));
    };
  }, [map, waypoints]);
};

const useDrawing = (enabled: boolean): DrawingCallbacks => {
  const [coords, setCoords] = useState<RouteCoords>([]);
  const map = useMap();

  const [d, dp] = useDrawOnMap(coords);
  useDrawWaypointsOnMap(coords);

  useEffect(() => {
    if (!enabled) {
      setCoords([]);
    }
  }, [enabled]);

  if (enabled) {
    return {
      addPoint: () => {
        const center = map.getCenter();
        setCoords((c) => [...c, [center.lat, center.lng, '']]);
      },
      undoPoint: () => {
        setCoords((c) => c.slice(0, -1));
      },
      addNote: (text: string) => {
        setCoords((c) => {
          if (!c.length) {
            return c;
          }
          const [lat, lng] = c[c.length - 1];
          return [...c.slice(0, -1), [lat, lng, text]];
        });
      },
      getCoords: () => coords,
      getLastNote: () => {
        if (!coords.length) {
          return null;
        }
        return coords[coords.length - 1][2];
      },
      distance: d,
      pendingDistance: dp,
    };
  }
  return {
    addPoint: () => undefined,
    undoPoint: () => undefined,
    addNote: (_: string) => undefined,
    getCoords: () => coords,
    getLastNote: () => null,
    distance: 0,
    pendingDistance: 0,
  };
};

export default useDrawing;
