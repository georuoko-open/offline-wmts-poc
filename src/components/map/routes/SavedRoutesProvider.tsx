import { FC } from 'types';
import { createContext } from 'preact';
import { useContext, useEffect, useState } from 'preact/hooks';

type SavedRoutes = [string, boolean, [number, number, string][]][];

const isValidSavedRoutes = (s: unknown): s is SavedRoutes => {
  return Array.isArray(s);
};

const getSavedRoutes = (): SavedRoutes => {
  try {
    const routes = window.localStorage.getItem('routes');
    if (routes) {
      const parsedRoutes = JSON.parse(routes) as unknown;
      if (isValidSavedRoutes(parsedRoutes)) {
        return parsedRoutes;
      }
    }
  } catch (error) {
    // swallow errors
  }
  return [];
};

const saveRoutes = (s: SavedRoutes) => {
  try {
    window.localStorage.setItem('routes', JSON.stringify(s));
  } catch (error) {
    // swallow errors
  }
};

const SavedRoutesContext = createContext<SavedRoutes>([]);

const SetSavedRoutesContext = createContext<
  (next: SavedRoutes | ((prev: SavedRoutes) => SavedRoutes)) => void
>((_) => {
  throw new Error('SetSavedRoutesContext used without provider');
});

export const useSavedRoutes = (): SavedRoutes => useContext(SavedRoutesContext);

export const useSetSavedRoutes = (): ((
  next: SavedRoutes | ((prev: SavedRoutes) => SavedRoutes),
) => void) => useContext(SetSavedRoutesContext);

const SavedRoutesProvider: FC = ({ children }) => {
  const [savedRoutes, setSavedRoutes] = useState<SavedRoutes>(getSavedRoutes);

  useEffect(() => {
    saveRoutes(savedRoutes);
  }, [savedRoutes]);

  return (
    <SetSavedRoutesContext.Provider value={setSavedRoutes}>
      <SavedRoutesContext.Provider value={savedRoutes}>
        {children}
      </SavedRoutesContext.Provider>
    </SetSavedRoutesContext.Provider>
  );
};

export default SavedRoutesProvider;
