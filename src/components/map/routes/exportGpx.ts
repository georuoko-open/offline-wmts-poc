import { etrsTm35FinNEToLatLng } from '../config';

const GPX_HEAD = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx
  version="1.1"
  creator="${window.location.href}"
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
>`;

const gpxWaypointsFromCoords = (waypoints: [number, number, string][]) => {
  return waypoints.map(([y, x, text]) => {
    const [lat, lng] = etrsTm35FinNEToLatLng(y, x);
    return [
      `<wpt lat="${lat}" lon="${lng}">`,
      `<name>${text}</name>`,
      `</wpt>`,
    ].join('\n');
  });
};

const gpxTrackFromCoords = (
  name: string,
  coords: [number, number][],
): string => {
  const parts = ['<trk>', `<name>${name}</name>`, '<trkseg>'];
  coords.forEach(([y, x]) => {
    const [lat, lng] = etrsTm35FinNEToLatLng(y, x);
    parts.push(`<trkpt lat="${lat}" lon="${lng}"></trkpt>`);
  });
  parts.push('</trkseg>');
  parts.push('</trk>');
  return parts.join('\n');
};

const GPX_TAIL = `</gpx>`;

const coordsToGpxString = (
  name: string,
  coords: [number, number, string][],
): string =>
  [
    GPX_HEAD,
    ...gpxWaypointsFromCoords(coords.filter(([, , text]) => text != '')),
    gpxTrackFromCoords(
      name,
      coords.map(([y, x]) => [y, x]),
    ),
    GPX_TAIL,
  ].join('\n') + '\n';

export const downloadAsGpx = (
  name: string,
  coords: [number, number, string][],
): void => {
  const link = document.createElement('a');
  link.style.display = 'none';
  const gpxString = coordsToGpxString(name, coords);
  const gpxUtf8Bytes = new TextEncoder().encode(gpxString);
  const gpxUtf8Blob = new Blob([gpxUtf8Bytes], { type: 'application/gpx+xml' });
  const url = window.URL.createObjectURL(gpxUtf8Blob);
  link.setAttribute('href', url);
  link.setAttribute('download', `${name}.gpx`);
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  window.URL.revokeObjectURL(url);
};
