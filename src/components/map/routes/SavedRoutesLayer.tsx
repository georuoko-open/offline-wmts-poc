import { useSavedRoutes } from './SavedRoutesProvider';
import { FC } from 'types';
import { Marker, Polyline, Popup } from 'react-leaflet';

const SavedRoutesLayer: FC = () => {
  const routes = useSavedRoutes();

  return (
    <>
      {routes
        .filter(([, enabled]) => enabled)
        .map(([n, , coords], i) => (
          <Polyline
            key={`l-${n}-${i}`}
            positions={coords.map(([lat, lon]) => [lat, lon])}
            pathOptions={{ color: 'red' }}
          />
        ))}
      {routes
        .filter(([, enabled]) => enabled)
        .flatMap(([n, , coords], ri) =>
          coords
            .filter(([, , text]) => text != '')
            .map(([lat, lon, text], ci) => (
              <Marker key={`m-${n}-${ri}-${ci}`} position={[lat, lon]}>
                <Popup>{text}</Popup>
              </Marker>
            )),
        )}
    </>
  );
};

export default SavedRoutesLayer;
