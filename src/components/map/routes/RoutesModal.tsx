import { FC } from 'types';
import { useState } from 'preact/hooks';
import { createPortal } from 'preact/compat';

import CloseIcon from 'icons/close.svg?inline';
import SaveIcon from 'icons/save.svg?inline';
import AddIcon from 'icons/add-marker.svg?inline';
import EditIcon from 'icons/edit-marker.svg?inline';
import UndoIcon from 'icons/undo.svg?inline';

import useDrawing from './useDrawing';
import { useSavedRoutes, useSetSavedRoutes } from './SavedRoutesProvider';

import s from './RoutesModal.scss';
import { downloadAsGpx } from './exportGpx';

const DisplayedRouteItem: FC<{
  name: string;
  enabled: boolean;
  onEnabledToggled: () => void;
  onDeleted: () => void;
  coords: [number, number, string][];
}> = ({ name, enabled, onEnabledToggled, onDeleted, coords }) => {
  const onDownloadGpx = () => {
    downloadAsGpx(name, coords);
  };

  return (
    <div>
      <input type="checkbox" checked={enabled} onChange={onEnabledToggled} />
      <span>{name}</span>
      <button onClick={onDeleted} style={{ marginLeft: 5 }}>
        Poista
      </button>
      <button onClick={onDownloadGpx} style={{ marginLeft: 5 }}>
        Lataa GPX
      </button>
    </div>
  );
};

const CenterMarker: FC = () => {
  return (
    <>
      <div
        style={{
          pointerEvents: 'none',
          position: 'absolute',
          top: 'calc(50% - 20px + 25px - 1px)',
          left: 'calc(50% - 20px - 1px)',
          width: 42,
          height: 42,
          minHeight: 0,
          zIndex: 9999,
          border: '7px solid grey',
          borderRadius: 21,
          background: 'none',
        }}
      />
      <div
        style={{
          pointerEvents: 'none',
          position: 'absolute',
          top: 'calc(50% - 20px + 25px)',
          left: 'calc(50% - 20px)',
          width: 40,
          height: 40,
          minHeight: 0,
          zIndex: 10000,
          border: '5px solid red',
          borderRadius: 20,
          background: 'none',
        }}
      />
      <div
        style={{
          pointerEvents: 'none',
          position: 'absolute',
          top: 'calc(50% - 40px + 25px)',
          left: 'calc(50% - 1px)',
          width: 2,
          height: 80,
          minHeight: 0,
          zIndex: 10000,
          border: '1px solid red',
          background: 'none',
        }}
      />
      <div
        style={{
          pointerEvents: 'none',
          position: 'absolute',
          top: 'calc(50% - 1px + 25px)',
          left: 'calc(50% - 40px)',
          width: 80,
          height: 2,
          minHeight: 0,
          zIndex: 10000,
          border: '1px solid red',
          background: 'none',
        }}
      />
    </>
  );
};

const RoutesModal: FC<{
  onClose: () => void;
}> = ({ onClose }) => {
  const savedRoutes = useSavedRoutes();
  const setSavedRoutes = useSetSavedRoutes();
  const [isDrawing, setIsDrawing] = useState(false);

  const {
    addPoint,
    undoPoint,
    addNote,
    getLastNote,
    getCoords,
    distance,
    pendingDistance,
  } = useDrawing(isDrawing);

  if (isDrawing) {
    return (
      <>
        {createPortal(
          <>
            <div className={s.toolbar}>
              <button aria-label="Lisää" onClick={addPoint}>
                <AddIcon />
              </button>
              <button
                aria-label="Lisää merkintä edelliseen"
                onClick={() => {
                  const last = getLastNote();
                  if (last != null) {
                    const next = prompt(
                      'Lisää edelliseen pisteeseen merkintäteksti',
                      last,
                    );
                    if (next != null) {
                      addNote(next);
                    }
                  }
                }}
              >
                <EditIcon />
              </button>
              <button aria-label="Poista edellinen" onClick={undoPoint}>
                <UndoIcon />
              </button>

              <div
                style={{
                  fontSize: 12,
                  paddingTop: 2,
                  paddingRight: 5,
                  textAlign: 'right',
                  marginLeft: 'auto',
                }}
              >
                {(distance / 1000).toFixed(3)}&nbsp;km
                <br />
                +&nbsp;{(pendingDistance / 1000).toFixed(3)}&nbsp;km
              </div>
              <button
                aria-label="Tallenna"
                onClick={(e) => {
                  e.stopPropagation();
                  const coords = getCoords();
                  if (!coords.length) {
                    return;
                  }
                  const name = prompt('Anna tallennettavan reitin nimi', '');
                  if (name == null) {
                    return;
                  }
                  setSavedRoutes((r) => [...r, [name, true, coords]]);
                  setIsDrawing(false);
                }}
              >
                <SaveIcon />
              </button>
              <button
                aria-label="Lopeta"
                onClick={(e) => {
                  e.stopPropagation();
                  setIsDrawing(false);
                }}
              >
                <CloseIcon />
              </button>
            </div>
            <CenterMarker />
          </>,
          document.body,
        )}
      </>
    );
  }

  return createPortal(
    <div className={s.backdrop} onClick={onClose}>
      <div className={s.modal} onClick={(e) => e.stopPropagation()}>
        <div className={s.banner}>
          <h2>Tallennetut reitit</h2>
          <button onClick={onClose} aria-label="Sulje ikkuna">
            <CloseIcon />
          </button>
        </div>
        <div style={{ paddingTop: 5, paddingBottom: 5 }}>
          <button onClick={() => setIsDrawing(true)}>Piirrä uusi reitti</button>
        </div>
        <div style={{ paddingBottom: 10 }}>
          {!!savedRoutes.length &&
            savedRoutes.map(([name, enabled, coords], i) => (
              <DisplayedRouteItem
                key={i}
                name={name}
                enabled={enabled}
                onDeleted={() => {
                  setSavedRoutes((rs) => {
                    return [...rs.slice(0, i), ...rs.slice(i + 1)];
                  });
                }}
                onEnabledToggled={() => {
                  setSavedRoutes((rs) =>
                    Array.from(rs, (r, ri) => {
                      if (ri === i) {
                        r[1] = !enabled;
                      }
                      return r;
                    }),
                  );
                }}
                coords={coords}
              />
            ))}
        </div>
      </div>
    </div>,
    document.body,
  );
};

export default RoutesModal;
