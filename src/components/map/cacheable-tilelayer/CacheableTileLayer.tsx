import { FC } from 'types';
import { useEffect } from 'preact/hooks';
import { useMap } from 'react-leaflet';
import {
  DomEvent,
  DomUtil,
  GridLayer,
  Point,
  TileErrorEvent,
  TileLayer,
  TileLayerOptions,
  Util,
} from 'leaflet';
import { load, save } from 'components/tilecache/operations';

import notAvailableTileUrl from './placeholder-tiles/not-available.png';
import notAvailableOfflineTileUrl from './placeholder-tiles/not-available-offline.png';

/**
 * Called in the createTile function of the tile layer.
 *
 * Should asyncronously set src property for the image element recived as argument, eg. tile.src = 'url'.
 */
const setSrcToTileLater = async (
  tile: HTMLImageElement,
  tileCoords: { x: number; y: number; z: number },
  tileUrl: string,
) => {
  const { x, y, z } = tileCoords;

  try {
    const cachedImageBlob = await load(x, y, z);
    if (cachedImageBlob) {
      if (tile.parentNode) {
        tile.src = URL.createObjectURL(cachedImageBlob);
      }
      return;
    }
  } catch (error) {
    // swallow errors when loading from cache and try from network instead
  }

  if (!tile.parentNode) {
    return;
  }

  let response: Response;

  try {
    response = await fetch(tileUrl);
  } catch (error) {
    // expect this to catch network errors when fetch itself throws and show a "not available without network" tile
    if (tile.parentNode) {
      tile.src = notAvailableOfflineTileUrl;
    }
    return;
  }

  if (!tile.parentNode) {
    return;
  }

  if (!response.ok) {
    tile.src = notAvailableTileUrl;
    return;
  }

  let blob: Blob;

  try {
    blob = await response.blob();
  } catch (error) {
    // show a "not available" tile for anything else than network errors
    if (tile.parentNode) {
      tile.src = notAvailableTileUrl;
    }
    return;
  }

  save(x, y, z, blob).catch(() => {
    // swallow errors when saving to cache
  });

  if (tile.parentNode) {
    tile.src = URL.createObjectURL(blob);
  }
};

/* eslint-disable eslint-comments/no-unlimited-disable */
/* eslint-disable */
TileLayer.include({
  _abortLoading: function () {
    let i, tile;
    let x = 0;
    let y = 0;
    for (i in this._tiles) {
      if (this._tiles[i].coords.z !== this._tileZoom) {
        x += 1;
        tile = this._tiles[i].el;
        tile.onload = Util.falseFn;
        tile.onerror = Util.falseFn;
        if (!tile.complete) {
          y += 1;
          const currentUrl = tile.src;
          tile.src = Util.emptyImageUrl;
          URL.revokeObjectURL(currentUrl);
          DomUtil.remove(tile);
          delete this._tiles[i];
        }
      }
    }
  },

  createTile: function (
    coords: Point,
    done: (errorEvent: TileErrorEvent | null, tile: HTMLImageElement) => void,
  ) {
    const tile = document.createElement('img');
    tile.alt = '';
    tile.setAttribute('role', 'presentation');

    DomEvent.on(tile, 'load', Util.bind(this._tileOnLoad, this, done, tile));
    DomEvent.on(tile, 'error', Util.bind(this._tileOnError, this, done, tile));

    const tileCoords = {
      x: coords.x,
      y: coords.y,
      z: this._getZoomForUrl() as number,
    };
    const tileUrl = this.getTileUrl(coords) as string;

    setSrcToTileLater(tile, tileCoords, tileUrl);

    return tile;
  },

  _removeTile: function (key: string) {
    const tile = this._tiles[key];
    if (!tile) {
      return;
    }

    // revoke the url here also since it is possible onload has not fired yet
    // and onload will be unbound after calling the prototype method
    const currentSrc = tile.el.getAttribute('src');
    tile.el.setAttribute('src', Util.emptyImageUrl);
    URL.revokeObjectURL(currentSrc);

    return (GridLayer.prototype as any)._removeTile.call(this, key);
  },

  _tileOnLoad: function (
    done: (errorEvent: TileErrorEvent | null, tile: HTMLImageElement) => void,
    tile: HTMLImageElement,
  ) {
    URL.revokeObjectURL(tile.src);
    done(null, tile);
  },

  _tileOnError: function (
    done: (errorEvent: TileErrorEvent | null, tile: HTMLImageElement) => void,
    tile: HTMLImageElement,
    e: TileErrorEvent,
  ) {
    URL.revokeObjectURL(tile.src);
    const errorUrl = this.options.errorTileUrl;
    if (errorUrl && tile.getAttribute('src') !== errorUrl) {
      tile.src = errorUrl;
    }
    done(e, tile);
  },
});
/* eslint-enable */

// use only a necessary subset of attributes from the tile layer since these
// are expanded in the useEffect hook to reload layer on props change
type CacheableTileLayerProps = {
  url: string;
} & Pick<TileLayerOptions, 'maxNativeZoom' | 'minNativeZoom' | 'attribution'>;

const CacheableTileLayer: FC<CacheableTileLayerProps> = (props) => {
  const { url, maxNativeZoom, minNativeZoom, attribution } = props;
  const map = useMap();

  // possibly not the optimal way to add and remove layer if the props change
  useEffect(() => {
    const layer = new TileLayer(url, {
      maxNativeZoom,
      minNativeZoom,
      attribution,
      detectRetina: true,
    });

    layer.addTo(map);

    return () => {
      layer.removeFrom(map);
    };
  }, [map, url, maxNativeZoom, minNativeZoom, attribution]);

  return null;
};

export default CacheableTileLayer;
