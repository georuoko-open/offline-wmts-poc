import { FC } from 'types';
import { useMap } from 'react-leaflet';

import LockClosedIcon from 'icons/lock-closed.svg?inline';
import LockOpenIcon from 'icons/lock-open.svg?inline';

import s from './LockZoomButton.scss';

const LockZoomButton: FC<{
  lockedZoom: number | null;
  setLockedZoom: (z: number | null) => void;
}> = ({ lockedZoom, setLockedZoom }) => {
  const map = useMap();

  return (
    <>
      <div className={s.wrapper}>
        <button
          onClick={() => {
            if (lockedZoom != null) {
              setLockedZoom(null);
            } else {
              setLockedZoom(map.getZoom());
            }
          }}
        >
          {lockedZoom != null ? (
            <LockClosedIcon fill="grey" />
          ) : (
            <LockOpenIcon fill="grey" />
          )}
        </button>
      </div>
    </>
  );
};

export default LockZoomButton;
