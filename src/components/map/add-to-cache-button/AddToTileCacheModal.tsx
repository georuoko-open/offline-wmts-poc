import { FC } from 'types';
import { LatLngBounds } from 'leaflet';
import { useState } from 'preact/hooks';
import { createPortal } from 'preact/compat';
import { useAddTileCacheArea } from 'components/tilecache/TileCacheSettingsProvider';
import uniqueId from 'utils/uniqueId';

import CloseIcon from 'icons/close.svg?inline';

import s from './AddToTileCacheModal.scss';

export type AddToTileCacheModalData = {
  bounds: LatLngBounds;
  tiles: {
    min: { x: number; y: number };
    max: { x: number; y: number };
    zoom: number;
  }[];
};

const AddToTileCacheModal: FC<{
  onClose: () => void;
  data: AddToTileCacheModalData;
}> = ({ onClose, data }) => {
  const toCacheTileCount = data.tiles.reduce(
    (s, t) => s + (t.max.x - t.min.x) * (t.max.y - t.min.y),
    0,
  );
  const toCacheApproxSizeMbs = toCacheTileCount * 0.02;

  const [name, setName] = useState('');
  const addArea = useAddTileCacheArea();

  const onSaveAreaClick = () => {
    const d = new Date();
    addArea({
      id: uniqueId(),
      timestamp: `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`,
      name,
      bounds: {
        n: data.bounds.getNorth(),
        w: data.bounds.getWest(),
        s: data.bounds.getSouth(),
        e: data.bounds.getEast(),
      },
      tiles: data.tiles,
    });
    onClose();
  };

  return createPortal(
    <div className={s.backdrop} onClick={onClose}>
      <div className={s.modal} onClick={(e) => e.stopPropagation()}>
        <div className={s.banner}>
          <h2>Tallenna kartta</h2>
          <button onClick={onClose} aria-label="Sulje ikkuna">
            <CloseIcon />
          </button>
        </div>
        {toCacheApproxSizeMbs > 400 ? (
          <div>
            <p>
              Ladattavan alue on liian iso. Latauksen koko olisi noin{' '}
              {toCacheApproxSizeMbs.toFixed(2)} megatavua. Tallenna alue
              useammassa pienemmässä osassa.
            </p>
          </div>
        ) : (
          <div>
            <p>
              Kartan näkyvä alue tallentaan laitteellesi. Latauksen koko on noin{' '}
              {toCacheApproxSizeMbs.toFixed(2)} megatavua. Latauksen jälkeen
              alueen kartta on käytettävissä myös ilman verkkoyhteyttä.
            </p>
            <p>
              Voit seurata tallennettuja alueita ja latauksen tilaa asetuksista.
              Näät tallennetut alueet kartalla valitsemalla ✓-symbolin kartalta.
            </p>
            <form>
              <label>
                <h3>Anna tallennettavalle alueelle nimi</h3>
                <input
                  type="text"
                  value={name}
                  onChange={(e) =>
                    setName((e.target as HTMLInputElement).value)
                  }
                />
              </label>
              <button
                type="submit"
                onClick={(e) => {
                  e.preventDefault();
                  onSaveAreaClick();
                }}
              >
                Tallenna
              </button>
            </form>
          </div>
        )}
      </div>
    </div>,
    document.body,
  );
};

export default AddToTileCacheModal;
