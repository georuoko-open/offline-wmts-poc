import { FC } from 'types';
import { useState } from 'preact/hooks';
import { Bounds } from 'leaflet';
import { useMap } from 'react-leaflet';
import { MAP_URL_MAX_NATIVE_ZOOM_LEVEL } from '../config';
import AddToTileCacheModal, {
  AddToTileCacheModalData,
} from './AddToTileCacheModal';

import DownloadIcon from 'icons/download.svg?inline';

import s from './AddToTileCacheButton.scss';

const getTilesInCurrentView = (
  currentPixelBounds: Bounds,
  currentZoom: number,
) => {
  const tr = currentPixelBounds.getTopRight();
  const bl = currentPixelBounds.getBottomLeft();
  return Array.from({ length: MAP_URL_MAX_NATIVE_ZOOM_LEVEL + 1 }, (_, z) => {
    // the max tile number possible for this zoom level is 2^z, 1 for 0, 2 for 1, 4 for 2, etc.
    const maxTile = Math.pow(2, z);
    // since pixel bounds are for the current zoom, get the pixels for this level with powers of two
    const tileNumber = (pixels: number) =>
      Math.floor((pixels * Math.pow(2, z - currentZoom)) / 256);
    // resolve the visible tiles for the loop zoom level the initial view
    return {
      zoom: z,
      min: {
        x: Math.max(tileNumber(bl.x), 0),
        y: Math.max(tileNumber(tr.y), 0),
      },
      max: {
        x: Math.min(tileNumber(tr.x), maxTile),
        y: Math.min(tileNumber(bl.y), maxTile),
      },
    };
  });
};

const AddToTileCacheButton: FC = () => {
  const map = useMap();

  const [modalData, setModalData] = useState<
    AddToTileCacheModalData | undefined
  >(undefined);

  const onClick = () => {
    const bounds = map.getBounds();
    const tiles = getTilesInCurrentView(map.getPixelBounds(), map.getZoom());
    setModalData({ bounds, tiles });
  };

  return (
    <>
      <div className={s.wrapper}>
        <button onClick={onClick}>
          <DownloadIcon />
        </button>
      </div>
      {!!modalData && (
        <AddToTileCacheModal
          data={modalData}
          onClose={() => setModalData(undefined)}
        />
      )}
    </>
  );
};

export default AddToTileCacheButton;
