import {
  Bounds,
  CRS,
  LatLng,
  LatLngBounds,
  LatLngBoundsLiteral,
  LatLngTuple,
  Point,
  Transformation,
} from 'leaflet';

// configure the map url here since its used in the tile layer downloader and in the cache downloader

export const generateMapUrl = (
  apikey: string,
  x: number | string = '{x}',
  y: number | string = '{y}',
  z: number | string = '{z}',
): string =>
  `https://avoin-karttakuva.maanmittauslaitos.fi/avoin/wmts/1.0.0/maastokartta/default/ETRS-TM35FIN/${z}/${y}/${x}.png?api-key=${apikey}`;

export const MAP_URL_MAX_NATIVE_ZOOM_LEVEL = 13; // nls open wmts has tiles for levels 0-13

// demo uses a simple ETRS-TM35FIN (EPSG:3067) coordinate system + tileset
// use these custom CRS hacks instead of proj4leaflet since everything is EPSG:3067 anyways

// use a simple math method to convert geolocation latlng to ne since map only knows ne coords
// constants and algorithm sourced from JHS 197 appendices 2 and 3
// https://www.suomidigi.fi/ohjeet-ja-tuki/jhs-suositukset/jhs-197-euref-fin-koordinaattijarjestelmat-niihin-liittyvat-muunnokset-ja-karttalehtijako

const e = 0.08181919104342208;
const h1d = 0.000837731824734;
const h2d = 0.000000760852779;
const h3d = 0.000000001197638;
const h4d = 0.000000000002443;
const h1 = 0.000837732168164;
const h2 = 0.000000059058696;
const h3 = 0.000000000167349;
const h4 = 0.000000000000217;
const degToRad = 0.017453292519943295;
const lam0rad = 27 * degToRad;

const A1 = 6367449.145771;
const k0 = 0.9996;
const E0 = 500000;

export const latLngToEtrsTm35FinNE = (
  lat: number,
  lng: number,
): LatLngTuple => {
  const phiRad = lat * degToRad;
  const lamRad = lng * degToRad;
  const Qd = Math.asinh(Math.tan(phiRad));
  const Qd2 = Math.atanh(e * Math.sin(phiRad));
  const beta = Math.atan(Math.sinh(Qd - e * Qd2));
  const nuud = Math.atanh(Math.cos(beta) * Math.sin(lamRad - lam0rad));
  const epsd = Math.asin(Math.sin(beta) * Math.cosh(nuud));
  const eps1 = h1d * Math.sin(2 * epsd) * Math.cosh(2 * nuud);
  const eps2 = h2d * Math.sin(4 * epsd) * Math.cosh(4 * nuud);
  const eps3 = h3d * Math.sin(6 * epsd) * Math.cosh(6 * nuud);
  const eps4 = h4d * Math.sin(8 * epsd) * Math.cosh(8 * nuud);
  const nuu1 = h1d * Math.cos(2 * epsd) * Math.sinh(2 * nuud);
  const nuu2 = h2d * Math.cos(4 * epsd) * Math.sinh(4 * nuud);
  const nuu3 = h3d * Math.cos(6 * epsd) * Math.sinh(6 * nuud);
  const nuu4 = h4d * Math.cos(8 * epsd) * Math.sinh(8 * nuud);
  const eps = epsd + eps1 + eps2 + eps3 + eps4;
  const nuu = nuud + nuu1 + nuu2 + nuu3 + nuu4;
  const A1 = 6367449.145771;
  const k0 = 0.9996;
  const E0 = 500000;
  const northing = A1 * eps * k0;
  const easting = A1 * nuu * k0 + E0;
  return [Math.round(northing), Math.round(easting)] as LatLngTuple;
};

export const etrsTm35FinNEToLatLng = (
  northing: number, // y
  easting: number, // x
): LatLngTuple => {
  const eps = northing / (A1 * k0);
  const nuu = (easting - E0) / (A1 * k0);
  const eps1d = h1 * Math.sin(2 * eps) * Math.cosh(2 * nuu);
  const eps2d = h2 * Math.sin(4 * eps) * Math.cosh(4 * nuu);
  const eps3d = h3 * Math.sin(6 * eps) * Math.cosh(6 * nuu);
  const eps4d = h4 * Math.sin(8 * eps) * Math.cosh(8 * nuu);
  const nuu1d = h1 * Math.cos(2 * eps) * Math.sinh(2 * nuu);
  const nuu2d = h2 * Math.cos(4 * eps) * Math.sinh(4 * nuu);
  const nuu3d = h3 * Math.cos(6 * eps) * Math.sinh(6 * nuu);
  const nuu4d = h4 * Math.cos(8 * eps) * Math.sinh(8 * nuu);
  const epsd = eps - eps1d - eps2d - eps3d - eps4d;
  const nuud = nuu - nuu1d - nuu2d - nuu3d - nuu4d;
  const beta = Math.asin(Math.sin(epsd) / Math.cosh(nuud));
  const Q = Math.asinh(Math.tan(beta));
  let Qd = Q;
  Qd = Q + e * Math.atanh(e * Math.tanh(Q));
  Qd = Q + e * Math.atanh(e * Math.tanh(Qd));
  Qd = Q + e * Math.atanh(e * Math.tanh(Qd));
  Qd = Q + e * Math.atanh(e * Math.tanh(Qd));
  const phiRad = Math.atan(Math.sinh(Qd));
  const lamRad = lam0rad + Math.asin(Math.tanh(nuud) / Math.cos(beta));
  const phi = phiRad / degToRad;
  const lam = lamRad / degToRad;
  return [phi, lam] as LatLngTuple;
};

const boundsTopLeft = new Point(-548576, 8388608);
const boundsBottomRight = new Point(1548576, 6291456);
const transformation = new Transformation(1, 548576, -1, 8388608);
const zoom = (scale: number) => Math.log(8192 * scale) / Math.LN2; // 1 pixel per meter on level 13, 0.5 pixels per meter on 12, etc
const scale = (zoom: number) => Math.pow(2, zoom - 13);
const project = (latlng: LatLng) => new Point(latlng.lng, latlng.lat);
const unproject = (point: Point) => new LatLng(point.y, point.x);

export const EPSG_3067_CRS = {
  code: 'EPSG:3067',
  infinite: false,
  wrapLng: undefined,
  wrapLat: undefined,
  wrapLatLng: (latlng: LatLng) => latlng,
  distance: (latlng1: LatLng, latlng2: LatLng) => {
    const dx = latlng2.lng - latlng1.lng;
    const dy = latlng2.lat - latlng1.lat;
    return Math.sqrt(dx * dx + dy * dy);
  },
  zoom,
  scale,
  project,
  unproject,
  latLngToPoint: (latlng: LatLng, zoom: number) =>
    transformation.transform(project(latlng), scale(zoom)),
  pointToLatLng: (point: Point, zoom: number) =>
    unproject(transformation.untransform(point, scale(zoom))),
  getProjectedBounds: (zoom: number) =>
    new Bounds(
      transformation.transform(boundsTopLeft, scale(zoom)),
      transformation.transform(boundsBottomRight, scale(zoom)),
    ),
  wrapLatLngBounds: (bounds: LatLngBounds) => bounds,
} as CRS;

export const EPSG_3067_CRS_TILESET_BOUNDS = [
  [6291456, -548576],
  [8388608, 1548576],
] as LatLngBoundsLiteral;

export const MAP_DEFAULT_LOCATION = {
  center: [7155000, 415000] as LatLngTuple,
  zoom: 3,
};
