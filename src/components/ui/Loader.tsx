import { FC } from 'types';
import cn from 'classnames';

import s from './Loader.scss';
import us from 'utils/helpers.scss';

const Loader: FC<{
  className?: string;
  iconClassName?: string;
  text?: string;
  textClassName?: string;
  delayVisibility?: boolean;
  inline?: boolean;
}> = ({
  className,
  iconClassName,
  text,
  textClassName,
  delayVisibility = false,
  inline = false,
}) => {
  return (
    <>
      <span
        className={cn(s.loadingIcon, className, iconClassName, {
          [s.delayVisible]: delayVisibility,
        })}
        style={inline ? { display: 'inline-block' } : undefined}
        aria-hidden={true}
      />
      <span
        className={cn(
          {
            [us.srOnly]: !text,
            [s.loadingText]: !!text,
          },
          className,
          textClassName,
        )}
      >
        {text || 'Sisältöä ladataan'}
      </span>
    </>
  );
};

export default Loader;
