import { useEffect, useState } from 'preact/hooks';
import { generateCacheKey, keys } from '../operations';
import { useTileCacheAreas } from '../TileCacheSettingsProvider';

type TileCacheDownloadStatus = {
  [areaId: string]: number | null;
};

const useTileCacheDownloadStatus = (): TileCacheDownloadStatus => {
  const currentAreas = useTileCacheAreas();
  const [currentKeys, setCurrentKeys] = useState<Set<string> | null>(null);

  // use a simple object as the reload signal for idb keys getter
  const [reload, setReload] = useState({});

  useEffect(() => {
    const timer = window.setInterval(() => setReload({}), 10000);
    return () => window.clearInterval(timer);
  }, []);

  useEffect(() => {
    let shouldUpdate = true;
    keys()
      .then((ks) => {
        if (shouldUpdate) {
          setCurrentKeys(new Set(ks));
        }
      })
      .catch(() => {
        if (shouldUpdate) {
          setCurrentKeys(new Set());
        }
      });
    return () => {
      shouldUpdate = false;
    };
  }, [reload]);

  if (currentKeys === null) {
    return Object.fromEntries(currentAreas.map(({ id }) => [id, null]));
  }

  return Object.fromEntries(
    currentAreas.map(({ id, tiles, totalTilesCount }) => {
      const tilesInCache = tiles.reduce((s, t) => {
        let collector = 0;
        for (let x = t.min.x; x <= t.max.x; x++) {
          for (let y = t.min.y; y <= t.max.y; y++) {
            if (currentKeys.has(generateCacheKey(x, y, t.zoom))) {
              collector++;
            }
          }
        }
        return s + collector;
      }, 0);

      return [id, Math.floor((100 * tilesInCache) / totalTilesCount)];
    }),
  );
};

export default useTileCacheDownloadStatus;
