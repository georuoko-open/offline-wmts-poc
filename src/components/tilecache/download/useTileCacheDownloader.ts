import { useEffect, useState } from 'preact/hooks';
import { useSettings } from 'components/settings/SettingsProvider';
import { generateMapUrl } from 'components/map/config';
import { useTileCacheAreas } from '../TileCacheSettingsProvider';
import { generateCacheKey, keys, saveMany } from '../operations';

const retryableRequestForTile = async (
  x: number,
  y: number,
  z: number,
  apikey: string,
) => {
  const theUrl = generateMapUrl(apikey, x, y, z);
  let retry = 0;
  while (retry <= 10) {
    if (retry > 0) {
      await new Promise((resolve) =>
        window.setTimeout(resolve, Math.min(1000 * 2 ** retry, 30000)),
      );
    }
    const theBlob = await fetch(theUrl)
      .then((r) => (r.ok ? r.blob() : undefined))
      .catch(() => undefined);
    if (theBlob) {
      return theBlob;
    }
    retry += 1;
  }
  throw new Error('unable to load tile from network');
};

const downloadTiles = async (
  apikey: string,
  tiles: [number, number, number][],
  shouldBreakRef: { current: boolean },
) => {
  const batchCount = 10;
  let batchStartIndex = 0;

  // bail out if the parent has set break ref
  while (!shouldBreakRef.current && batchStartIndex < tiles.length) {
    // pause for a short time before each batch not to bomb the api continuously
    await new Promise((resolve) => setTimeout(resolve, 250));

    // generate the current batch from the tiles list, taking batch count items after the current index
    const batchIndices = Array.from(
      { length: batchCount },
      (_, i) => batchStartIndex + i,
    ).filter((i) => i < tiles.length);

    const batchRequests = batchIndices
      // eslint-disable-next-line security/detect-object-injection
      .map((ii) => tiles[ii] as [number, number, number])
      .map(([x, y, z]) =>
        retryableRequestForTile(x, y, z, apikey).then(
          (b) => [[x, y, z], b] as [[number, number, number], Blob],
        ),
      );

    const recieved = await Promise.all(batchRequests);
    await saveMany(recieved);
    batchStartIndex += batchCount;
  }
};

const useDownloadTiles = (tiles: [number, number, number][]) => {
  const { apikey } = useSettings();

  useEffect(() => {
    // create a object ref to inform the downloader that this hook has unmounted
    const shouldBreakRef = { current: false };
    if (apikey) {
      downloadTiles(apikey, tiles, shouldBreakRef).catch(() => {
        // swallow errors and just stop the downloading
        // refresh the page to start again
      });
    }
    return () => {
      shouldBreakRef.current = true;
    };
  }, [apikey, tiles]);
};

const useTileCacheDownloader = (): void => {
  const currentAreas = useTileCacheAreas();
  const { apikey } = useSettings();
  const [toDownload, setToDownload] = useState<[number, number, number][]>([]);

  // when saved areas changes, get the keys in the db and set the list that needs downloading
  // also use the apikey as a dependency since changing it would reuse the previous list of tiles
  // and download them again
  useEffect(() => {
    let shouldUpdate = true;
    keys()
      .then((ks) => {
        if (shouldUpdate) {
          const currentKeys = new Set(ks);

          // use a set of strings to collect the tile xyz coords to ignore duplicates
          const collector = new Set<string>();

          currentAreas.forEach(({ tiles }) => {
            tiles.forEach((t) => {
              for (let x = t.min.x; x <= t.max.x; x++) {
                for (let y = t.min.y; y <= t.max.y; y++) {
                  if (!currentKeys.has(generateCacheKey(x, y, t.zoom))) {
                    collector.add(`${x}-${y}-${t.zoom}`);
                  }
                }
              }
            });
          });

          const PREFERRED_ZOOMS: { [k: number]: number } = {
            11: -3,
            9: -2,
            7: -1,
          };

          const sortByZoom = (
            a: [number, number, number],
            b: [number, number, number],
          ) => {
            const [, , z_a] = a;
            const [, , z_b] = b;
            return (
              // eslint-disable-next-line security/detect-object-injection
              (PREFERRED_ZOOMS[z_a] || z_a) - (PREFERRED_ZOOMS[z_b] || z_b)
            );
          };

          // extract the xyz coords from the set
          // sort the download array using some zooms as more preferred
          setToDownload(
            Array.from(
              collector,
              (s) =>
                s.split('-').map((i) => Number(i)) as [number, number, number],
            ).sort(sortByZoom),
          );
        }
      })
      .catch(() => {
        if (shouldUpdate) {
          setToDownload([]);
        }
      });
    return () => {
      shouldUpdate = false;
    };
  }, [currentAreas, apikey]);

  // pass the list of tiles to download to another hook
  useDownloadTiles(toDownload);
};

export default useTileCacheDownloader;
