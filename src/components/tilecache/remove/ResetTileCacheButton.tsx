import { FC } from 'types';
import { useEffect, useState } from 'preact/hooks';
import { useClearTileCacheAreas } from '../TileCacheSettingsProvider';
import { clear as clearBlobs } from '../operations';

const ResetTileCacheButton: FC = () => {
  const [isDeleting, setIsDeleting] = useState(false);

  const clearSavedAreas = useClearTileCacheAreas();

  useEffect(() => {
    if (!isDeleting) {
      return;
    }
    let mounted = true;
    clearBlobs()
      .then(() => {
        if (mounted) {
          clearSavedAreas();
        }
      })
      .catch(() => {
        // swallow errors
      });
    return () => {
      mounted = false;
      setIsDeleting(false);
    };
  }, [isDeleting, clearSavedAreas]);

  return (
    <>
      <button
        style={{ 'min-width': '90px' }}
        onClick={() => setIsDeleting(true)}
        disabled={isDeleting}
      >
        {isDeleting ? 'Poistetaan' : 'Poista kaikki'}
      </button>
      <span>(poistaa myös selatessa tallentuneet kartat)</span>
    </>
  );
};

export default ResetTileCacheButton;
