import { FC } from 'types';
import { useCallback, useEffect, useState } from 'preact/hooks';
import { MAP_URL_MAX_NATIVE_ZOOM_LEVEL } from 'components/map/config';
import {
  useRemoveTileCacheArea,
  useTileCacheAreas,
} from '../TileCacheSettingsProvider';
import { removeMany } from '../operations';

const isBetweenInclusive = (start: number, test: number, end: number) =>
  test >= start && test <= end;

const useRemoveTileCacheAreaTiles = (): ((aid: string) => Promise<void>) => {
  const areas = useTileCacheAreas();
  const removeTilesFromCacheFn = useCallback(
    (aid: string) => {
      // probably not the best logic to find the tiles used by this area only

      // populate the object with zoom level keys and empty lists
      const otherTilesByZoom = Object.fromEntries(
        Array.from({ length: MAP_URL_MAX_NATIVE_ZOOM_LEVEL + 1 }, (_, i) => [
          i,
          [] as {
            min: { x: number; y: number };
            max: { x: number; y: number };
          }[],
        ]),
      );

      // fill all other saved areas tiles to the object by zoom level
      areas
        .filter((a) => a.id !== aid)
        .forEach(({ tiles }) => {
          tiles.forEach((t) => {
            otherTilesByZoom[t.zoom]?.push({ min: t.min, max: t.max });
          });
        });

      const toRemove = [] as [number, number, number][];

      areas
        .find((a) => a.id === aid)
        // for every tile coordinate for this area, only remove it if the
        // coordinate is outside of every other areas coordinates
        ?.tiles.forEach((t) => {
          const othersForThisZoom = otherTilesByZoom[t.zoom];
          for (let x = t.min.x; x <= t.max.x; x++) {
            for (let y = t.min.y; y <= t.max.y; y++) {
              if (
                othersForThisZoom?.every(
                  (ot) =>
                    !(
                      isBetweenInclusive(ot.min.x, x, ot.max.x) &&
                      isBetweenInclusive(ot.min.y, y, ot.max.y)
                    ),
                )
              ) {
                toRemove.push([x, y, t.zoom]);
              }
            }
          }
        });
      return removeMany(toRemove);
    },
    [areas],
  );
  return removeTilesFromCacheFn;
};

const RemoveFromTileCacheButton: FC<{
  areaId: string;
}> = ({ areaId }) => {
  const [isDeleting, setIsDeleting] = useState(false);

  const removeTiles = useRemoveTileCacheAreaTiles();
  const removeArea = useRemoveTileCacheArea();

  useEffect(() => {
    if (!isDeleting) {
      return;
    }
    let mounted = true;
    removeTiles(areaId)
      .then(() => {
        // only remove the area from localstorage after the tiles are successfully removed
        if (mounted) {
          removeArea(areaId);
        }
      })
      .catch(() => {
        // swallow errors
      });
    return () => {
      mounted = false;
    };
  }, [areaId, isDeleting, removeArea, removeTiles]);

  return (
    <button
      style={{ 'min-width': '90px' }}
      onClick={() => setIsDeleting(true)}
      disabled={isDeleting}
    >
      {isDeleting ? 'Poistetaan' : 'Poista'}
    </button>
  );
};

export default RemoveFromTileCacheButton;
