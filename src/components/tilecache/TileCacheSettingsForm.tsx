import Loader from 'components/ui/Loader';
import { FC } from 'types';
import useTileCacheDownloadStatus from './download/useTileCacheDownloadStatus';
import RemoveFromTileCacheButton from './remove/RemoveFromTileCacheButton';
import ResetTileCacheButton from './remove/ResetTileCacheButton';
import { useTileCacheAreas } from './TileCacheSettingsProvider';

const CacheSizeInfoText: FC<{ total: number; shared: number }> = ({
  total,
  shared,
}) => {
  let info = `~ ${(total * 0.02).toFixed(2)} Mt`;
  if (shared > 100) {
    info += `, josta ${(shared * 0.02).toFixed(2)} Mt jaettu`;
  }
  return <>{info}</>;
};

const DownloadStatusInfoText: FC<{ loaded: number | null }> = ({ loaded }) => {
  if (loaded === null) {
    return (
      <>
        <Loader inline={true} /> Tarkistetaan
      </>
    );
  }
  return (
    <>
      {loaded < 100 && <Loader inline={true} />} {`${loaded} % ladattu`}
    </>
  );
};

const TileCacheSettingsForm: FC = () => {
  const areas = useTileCacheAreas();
  const downloadStatus = useTileCacheDownloadStatus();

  return (
    <>
      <h2>Tallennetut alueet</h2>
      {areas.length ? (
        <>
          <p>
            <ul>
              {areas.map((a) => (
                <li key={a.id}>
                  {a.name}
                  <ul>
                    <li>{a.timestamp}</li>
                    <li>
                      <CacheSizeInfoText
                        total={a.totalTilesCount}
                        shared={a.sharedTilesCount}
                      />
                    </li>
                    <li>
                      <DownloadStatusInfoText
                        loaded={downloadStatus[a.id] ?? null}
                      />
                    </li>
                    <li>
                      <RemoveFromTileCacheButton areaId={a.id} />
                    </li>
                  </ul>
                </li>
              ))}
            </ul>
          </p>
          {Object.values(downloadStatus).some(
            (s) => s !== null && s !== 100,
          ) && (
            <p>
              Lataustilanne päivittyy 10 sekunnin välein. Jos tilanne ei etene,
              päivitä sivu jotta lataus lähtee uudestaan käyntiin.
            </p>
          )}
        </>
      ) : (
        <p>Ei tallennettuja alueita.</p>
      )}
      <p>
        <ResetTileCacheButton />
      </p>
    </>
  );
};

export default TileCacheSettingsForm;
