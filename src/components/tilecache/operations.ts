import {
  getMany as idbGetMany,
  keys as idbKeys,
  clear as idbClear,
  setMany as idbSetMany,
  del as idbDel,
} from 'idb-keyval';

// simple pool to use getmany

class Deferred<T> {
  promise: Promise<T>;
  reject!: (v: unknown) => void;
  resolve!: (v: T) => void;

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.reject = reject;
      this.resolve = resolve;
    });
  }
}

let isFetching = false;
const tilesToFetch = new Map<string, Deferred<Blob | undefined>>();

const doPooledFetch = async () => {
  if (isFetching) {
    return;
  }

  isFetching = true;

  const keysToFetch = Array.from(tilesToFetch.keys()).slice(0, 100);

  await idbGetMany(keysToFetch)
    .then((blobsForKeys: (Blob | undefined)[]) => {
      keysToFetch
        // eslint-disable-next-line security/detect-object-injection
        .map((k, idx) => [k, blobsForKeys[idx]] as [string, Blob | undefined])
        .forEach(([key, blob]) => {
          tilesToFetch.get(key)?.resolve(blob);
          tilesToFetch.delete(key);
        });
    })
    .catch(() => {
      keysToFetch.forEach((k) => {
        tilesToFetch.get(k)?.reject(undefined);
        tilesToFetch.delete(k);
      });
    });

  if (tilesToFetch.size > 0) {
    void doPooledFetch();
  } else {
    isFetching = false;
  }
};

export const generateCacheKey = (x: number, y: number, z: number): string =>
  `1/${z}/${x}/${y}`;

export const load = async (
  x: number,
  y: number,
  z: number,
): Promise<Blob | undefined> => {
  const tileKey = generateCacheKey(x, y, z);
  const deferredTile = tilesToFetch.get(tileKey);
  if (deferredTile) {
    return deferredTile.promise;
  } else {
    const newDeferredTile = new Deferred<Blob | undefined>();
    tilesToFetch.set(tileKey, newDeferredTile);

    // defer the fetch with an immediate promise so there is a chance for other
    // synchronously called loads to add their respective tiles to the list, so the first
    // fetch does not run with a single tile only
    await Promise.resolve();
    void doPooledFetch();

    return newDeferredTile.promise;
  }
};

// simple pool to use setmany

let isSaving = false;
const toSaveTiles = new Map<string, Blob>();

const doPooledSave = async () => {
  if (isSaving) {
    return;
  }

  isSaving = true;

  const toSaveEntries = Array.from(toSaveTiles.entries()).slice(0, 100);

  await idbSetMany(toSaveEntries).catch(() => undefined);

  toSaveEntries.forEach(([key]) => {
    toSaveTiles.delete(key);
  });

  if (toSaveTiles.size > 0) {
    void doPooledSave();
  } else {
    isSaving = false;
  }
};

export const save = async (
  x: number,
  y: number,
  z: number,
  imageBlob: Blob,
): Promise<void> => {
  toSaveTiles.set(generateCacheKey(x, y, z), imageBlob);

  // defer the save with an immediate promise so there is a chance for other
  // synchronously called saves to add their respective tiles to the list, so the first
  // saves does not run with a single tile only
  await Promise.resolve();

  void doPooledSave();
};

export const saveMany = (
  entries: [[number, number, number], Blob][],
): Promise<void> => {
  return idbSetMany(
    entries.map(([[x, y, z], b]) => [generateCacheKey(x, y, z), b]),
  );
};

export const removeMany = (
  entries: [number, number, number][],
): Promise<void> => {
  return Promise.all(
    entries.map(([x, y, z]) => idbDel(generateCacheKey(x, y, z))),
  ).then(() => {
    return undefined;
  });
};

export const keys = (): Promise<string[]> => idbKeys() as Promise<string[]>;

export const count = (): Promise<number> => idbKeys().then((ks) => ks.length);

export const clear = (): Promise<void> => idbClear();
