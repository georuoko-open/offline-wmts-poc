import { FC } from 'types';
import { createContext } from 'preact';
import { StateUpdater, useContext, useEffect, useState } from 'preact/hooks';
import useTileCacheDownloader from './download/useTileCacheDownloader';

type TileCacheArea = {
  id: string;
  timestamp: string;
  name: string;
  bounds: { n: number; w: number; s: number; e: number };
  tiles: {
    zoom: number;
    min: { x: number; y: number };
    max: { x: number; y: number };
  }[];
};

type TileCacheAreaWithCounts = {
  totalTilesCount: number;
  sharedTilesCount: number;
} & TileCacheArea;

const isValidTileCacheAreas = (areas: unknown): areas is TileCacheArea[] => {
  const v = areas as TileCacheArea[];
  return (
    Array.isArray(v) &&
    v.every(
      (i) =>
        'id' in i &&
        'timestamp' in i &&
        'name' in i &&
        'bounds' in i &&
        'tiles' in i &&
        Array.isArray(i.tiles),
    ) &&
    v.map((i) => i.tiles.length).every((tl) => tl === v[0]?.tiles.length)
  );
};

const getAreasFromLocalStorage = (): TileCacheArea[] => {
  try {
    const storedAreas = window.localStorage.getItem('tilecache');
    if (storedAreas) {
      const parsedAreas = JSON.parse(storedAreas) as unknown;
      if (isValidTileCacheAreas(parsedAreas)) {
        return parsedAreas;
      }
    }
  } catch (error) {
    // swallow errors
  }
  return [];
};

const saveAreasToLocalStorage = (areas: TileCacheArea[]) => {
  try {
    window.localStorage.setItem('tilecache', JSON.stringify(areas));
  } catch (error) {
    // swallow errors
  }
};

const calculateOverlappingTileCount = (
  one: { min: { x: number; y: number }; max: { x: number; y: number } },
  two: { min: { x: number; y: number }; max: { x: number; y: number } },
) => {
  return (
    Math.max(
      0,
      Math.min(one.max.x, two.max.x) - Math.max(one.min.x, two.min.x),
    ) *
    Math.max(0, Math.min(one.max.y, two.max.y) - Math.max(one.min.y, two.min.y))
  );
};

const TileCacheAreasContext = createContext<TileCacheAreaWithCounts[]>([]);

const SetTileCacheAreasContext = createContext<StateUpdater<TileCacheArea[]>>(
  (_a) => {
    throw new Error('SetTileCacheAreasContext context used without provider');
  },
);

export const useTileCacheAreas = (): TileCacheAreaWithCounts[] =>
  useContext(TileCacheAreasContext);

export const useAddTileCacheArea = (): ((a: TileCacheArea) => void) => {
  const setAreas = useContext(SetTileCacheAreasContext);
  return (a: TileCacheArea) => {
    setAreas((as) => [...as, a]);
  };
};

export const useRemoveTileCacheArea = (): ((aid: string) => void) => {
  const setAreas = useContext(SetTileCacheAreasContext);
  return (aid: string) => {
    setAreas((as) => as.filter(({ id }) => id !== aid));
  };
};

export const useClearTileCacheAreas = (): (() => void) => {
  const setAreas = useContext(SetTileCacheAreasContext);
  return () => {
    setAreas([]);
  };
};

const Downloader: FC = () => {
  useTileCacheDownloader();
  return null;
};

const TileCacheProvider: FC = ({ children }) => {
  const [storedAreas, setStoredAreas] = useState<TileCacheArea[]>(
    getAreasFromLocalStorage,
  );

  useEffect(() => {
    saveAreasToLocalStorage(storedAreas);
  }, [storedAreas]);

  // probably not the best logic to find the tiles shared with others
  const areasWithCounts = storedAreas.map((a, _i, as) => {
    const thisAreaTiles = a.tiles;
    const sharedTilesCount = as.reduce((s, aa) => {
      if (aa.id === a.id) {
        return s;
      }
      const shared = aa.tiles.reduce((ss, otherTilesForZoom, i) => {
        // eslint-disable-next-line security/detect-object-injection
        const thisAreaTilesForZoom = thisAreaTiles[i];
        if (thisAreaTilesForZoom) {
          return (
            ss +
            calculateOverlappingTileCount(
              thisAreaTilesForZoom,
              otherTilesForZoom,
            )
          );
        }
        return ss;
      }, 0);
      return s + shared;
    }, 0);
    return {
      ...a,
      totalTilesCount: thisAreaTiles.reduce(
        (s, t) => s + (t.max.x - t.min.x + 1) * (t.max.y - t.min.y + 1),
        0,
      ),
      sharedTilesCount,
    };
  });

  return (
    <SetTileCacheAreasContext.Provider value={setStoredAreas}>
      <TileCacheAreasContext.Provider value={areasWithCounts}>
        <Downloader />
        {children}
      </TileCacheAreasContext.Provider>
    </SetTileCacheAreasContext.Provider>
  );
};

export default TileCacheProvider;
