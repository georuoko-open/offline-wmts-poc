import { FC } from 'types';
import { createContext } from 'preact';
import { useContext, useEffect, useState } from 'preact/hooks';

type Settings = {
  apikey: string;
};

const isValidSettings = (s: unknown): s is Settings => {
  return typeof ((s as Settings) || {}).apikey === 'string';
};

const SettingsContext = createContext<Settings>({ apikey: '' });

const SetSettingsContext = createContext<(s: Settings) => void>((_s) => {
  throw new Error('Settings context used without provider');
});

export const useSettings = (): Settings => useContext(SettingsContext);

export const useSetSettings = (): ((s: Settings) => void) =>
  useContext(SetSettingsContext);

const getSettingsFromLocalStorage = (): Settings => {
  try {
    const storedSettings = window.localStorage.getItem('settings');
    if (storedSettings) {
      const parsedSettings = JSON.parse(storedSettings) as unknown;
      if (isValidSettings(parsedSettings)) {
        return parsedSettings;
      }
    }
  } catch (error) {
    // swallow errors
  }
  return {
    apikey: '',
  };
};

const saveSettingsToLocalStorage = (s: Settings) => {
  try {
    window.localStorage.setItem('settings', JSON.stringify(s));
  } catch (error) {
    // swallow errors
  }
};

const SettingsProvider: FC = ({ children }) => {
  const [settings, setSettings] = useState<Settings>(
    getSettingsFromLocalStorage,
  );

  useEffect(() => {
    saveSettingsToLocalStorage(settings);
  }, [settings]);

  return (
    <SetSettingsContext.Provider value={setSettings}>
      <SettingsContext.Provider value={settings}>
        {children}
      </SettingsContext.Provider>
    </SetSettingsContext.Provider>
  );
};

export default SettingsProvider;
