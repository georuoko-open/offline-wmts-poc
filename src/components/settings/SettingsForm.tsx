import { FC } from 'types';
import { useState } from 'preact/hooks';
import { Link } from 'wouter-preact';
import Routes from 'config/routes';
import { useSetSettings, useSettings } from './SettingsProvider';

import s from './SettingsForm.scss';

const SettingsForm: FC = () => {
  const savedSettings = useSettings();
  const setSavedSettings = useSetSettings();

  const [formState, setFormState] = useState<{ apikey: string }>(savedSettings);

  const saveSettings = () => {
    setSavedSettings({ apikey: formState.apikey });
  };

  return (
    <>
      <p>
        Demon kartta ladataan Maanmittauslaitoksen Karttakuvapalvelun avoimesta
        rajapinnasta. Tarvitset demon käyttöä varten oman rajapinta-avaimen.
        Avain lisätään rajapintakutsuihin, ja se tallennetaan vain laitteellesi.
        Huomioithan rajapinta-avainta hankkiessa ja demon karttaa käyttäessä{' '}
        <a
          href="https://www.maanmittauslaitos.fi/kartat-ja-paikkatieto/asiantuntevalle-kayttajalle/maanmittauslaitoksen-avoimen-rajapinnan"
          target="_blank"
          rel="noopener noreferrer"
        >
          Maanmittauslaitoksen avoimen rajapinnan käyttöehdot
        </a>
        .
      </p>
      <p>
        Lisätietoja:
        <ul>
          <li>
            <a
              href="https://www.maanmittauslaitos.fi/rajapinnat/api-avaimen-ohje"
              target="_blank"
              rel="noopener noreferrer"
            >
              Maanmittauslaitoksen rajapinta-avain ja sen hankinta
            </a>
          </li>
          <li>
            <a
              href="https://www.maanmittauslaitos.fi/karttakuvapalvelu/tekninen-kuvaus-wmts#avoin-rajapintayhteys"
              target="_blank"
              rel="noopener noreferrer"
            >
              Maanmittauslaitoksen Karttakuvapalvelu
            </a>
          </li>
          <li>
            <Link to={Routes.ATTRIBUTION}>Aineiston lisenssi</Link>
          </li>
        </ul>
      </p>
      <form className={s.settingsForm}>
        <label>
          <h2>Rajapinta-avain</h2>
          <input
            type="text"
            value={formState.apikey}
            onInput={(e) =>
              setFormState({ apikey: (e.target as HTMLInputElement).value })
            }
          />
        </label>
        <button
          className={s.saveButton}
          type="submit"
          onClick={(e) => {
            e.preventDefault();
            saveSettings();
          }}
          disabled={formState.apikey === savedSettings.apikey}
        >
          Tallenna
        </button>
      </form>
    </>
  );
};

export default SettingsForm;
